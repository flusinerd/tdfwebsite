import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GeneralFittingComponent } from './general-fitting.component';

describe('GeneralFittingComponent', () => {
  let component: GeneralFittingComponent;
  let fixture: ComponentFixture<GeneralFittingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GeneralFittingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GeneralFittingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
