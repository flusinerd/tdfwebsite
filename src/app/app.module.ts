import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NewbroGuideComponent } from './newbro-guide/newbro-guide.component';
import { JoiningFleetComponent } from './joining-fleet/joining-fleet.component';
import { DpsFitsComponent } from './dps-fits/dps-fits.component';
import { HttpClientModule } from '@angular/common/http';
import { LogiFitsComponent } from './logi-fits/logi-fits.component';
import { SniperFitsComponent } from './sniper-fits/sniper-fits.component';
import { BoosterFitsComponent } from './booster-fits/booster-fits.component';
import { StatusComponent } from './status/status.component';
import { GeneralFittingComponent } from './general-fitting/general-fitting.component';
import { CalculatorComponent } from './calculator/calculator.component';
import { FormsModule } from '@angular/forms';
import { Browser } from 'protractor';
import { FleetstatComponent } from './fleetstat/fleetstat.component';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NewbroGuideComponent,
    JoiningFleetComponent,
    DpsFitsComponent,
    LogiFitsComponent,
    SniperFitsComponent,
    BoosterFitsComponent,
    StatusComponent,
    GeneralFittingComponent,
    CalculatorComponent,
    FleetstatComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    NgbModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
