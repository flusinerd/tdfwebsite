import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewbroGuideComponent } from './newbro-guide.component';

describe('NewbroGuideComponent', () => {
  let component: NewbroGuideComponent;
  let fixture: ComponentFixture<NewbroGuideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewbroGuideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewbroGuideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
