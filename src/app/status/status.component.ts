import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'incursion-status',
  templateUrl: './status.component.html',
  styleUrls: ['./status.component.scss']
})
export class StatusComponent implements OnInit {
  public loaded:boolean = false;
  constructor(private http:HttpClient) { }
  private focusStatus:any;
  public estimate:String;
  public focusName:String

  ngOnInit() {
    this.getJson();
  }

  private getJson() {
    this.http.get("../../assets/focus.json")
    .subscribe((data:any) => {
      this.focusName = data.focus.trim();
      let focus = encodeURIComponent(data.focus.trim())
      this.getCurrentFocusId(focus);
    })
  }

  private getCurrentFocusId (focus:String) {
    this.http.get(`https://esi.evetech.net/latest/search/?categories=constellation&datasource=tranquility&language=en-us&search=${focus}&strict=true`)
    .subscribe((data:any) => {
      this.getCurrentFocusStatus(data.constellation)
    })
  }

  private getCurrentFocusStatus(focusId:Number){
    this.http.get("https://esi.evetech.net/latest/incursions/?datasource=tranquility")
    .subscribe((data:Array<any>) => {
      data.forEach(incursion => {
        if(incursion.constellation_id != focusId){
          return
        }
        
        this.focusStatus = incursion.state
        if (this.focusStatus == "withdrawing"){
          this.estimate =  "Less then a day"
        } else if (this.focusStatus == "mobilizing"){
          this.estimate =  "Less then three days"
        }
        else if (this.focusStatus == "established"){
          this.estimate =  "At least 4 days"
        }
        this.loaded = true;
      });
    })
  }

}
