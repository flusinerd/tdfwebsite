import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BoosterFitsComponent } from './booster-fits.component';

describe('BoosterFitsComponent', () => {
  let component: BoosterFitsComponent;
  let fixture: ComponentFixture<BoosterFitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BoosterFitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BoosterFitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
