import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FleetstatComponent } from './fleetstat.component';

describe('FleetstatComponent', () => {
  let component: FleetstatComponent;
  let fixture: ComponentFixture<FleetstatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FleetstatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FleetstatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
