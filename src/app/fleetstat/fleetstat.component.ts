import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import {  ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-fleetstat',
  templateUrl: './fleetstat.component.html',
  styleUrls: ['./fleetstat.component.scss']
})
export class FleetstatComponent implements OnInit {
  public iskh;
  public totalearned;
  public totalrunning;
  public slowest;
  public fastest;
  public mean;
  private id;
  constructor(private _api:ApiService, private activatedRoute: ActivatedRoute, private router:Router) { 
    this.activatedRoute.params.subscribe((params) => {
      this.id = +params['id'];
      this.getStats();
    })
  }

  ngOnInit() {
  }

  private getStats(){
    this._api.getStats(this.id)
    .then((response) => {
      this.iskh = response.iskperhour;
      this.totalearned = response.totalearned;
      this.totalrunning = response.totaltime;
      this.slowest = response.slowest;
      this.fastest = response.fastest;
      this.mean = response.mean;
    })
    .catch((err) => {
      console.error(err);
    })
  }

  public newPaste(){
    this.router.navigate(['/fleetstats']);
  }
}
