import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogiFitsComponent } from './logi-fits.component';

describe('LogiFitsComponent', () => {
  let component: LogiFitsComponent;
  let fixture: ComponentFixture<LogiFitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogiFitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogiFitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
