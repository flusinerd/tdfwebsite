import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss']
})
export class CalculatorComponent implements OnInit {

  constructor(private _api: ApiService, private router: Router) { }


  public walletInput: String;
  public dates: Array<any> = [];
  public runningTime: any
  public totalEarned: number = 0;
  public runningTimeMs;
  public iskh;
  public iskhParsed;
  public totalEarnedParsed;
  public calculated: boolean = false;
  public totalSites = 0;
  public runningTimeString = ""
  public fastestSiteTime;
  public slowestSiteTime;
  public prevSiteTime: any;
  public avarageTime;
  public slowestTimeString = ""
  public fastestTimeString = ""
  public averageTimeString = ""
  public firstSitePayout;


  public calculatePayout() {
    if (this.walletInput != undefined) {
      // Reset Vars
      this.dates = [];
      this.totalEarned = 0;
      this.averageTimeString = "";
      this.fastestTimeString = "";
      this.slowestTimeString = "";
      this.runningTimeString = "";
      this.slowestSiteTime = undefined;
      this.fastestSiteTime = 0;
      this.prevSiteTime = undefined;
      this.totalSites = 0;

      let lines = this.walletInput.split(/\n/)

      lines.forEach(line => {
        if (line == "") {
          return;
        }
        let parts = line.split(/\s{2,}|\t/)
        if (parts[1] !== "Corporate Reward Payout") {
          return;
        }
        let payout = +(parts[2].replace(" ISK", "").replace(/\.|\,/g, ""))
        console.log(parts);
        let date = parts[0].replace(/\./g, "-")
        var datetime = new Date(date)
        this.dates.push(datetime)
        this.totalEarned += payout;
        this.totalSites++;
        if (this.firstSitePayout == undefined || this.firstSitePayout == null) {
          this.firstSitePayout = +(parts[2].replace(" ISK", "").replace(/\.|\,|\s/g, ""));
        }

        // Calculate site time
        // if prev is not set skip
        // prev - current
        // if res > slowest : slowest
        // if res < fastest : fastest
        // set this site time  = prev

        if (this.prevSiteTime != undefined) {
          let siteTime = this.prevSiteTime - datetime.getTime();
          if (siteTime > this.slowestSiteTime || this.slowestSiteTime == undefined) {
            this.slowestSiteTime = siteTime
          }
          if (siteTime < this.fastestSiteTime || this.fastestSiteTime == undefined || this.fastestSiteTime == 0) {
            this.fastestSiteTime = siteTime
          }
        }
        this.prevSiteTime = datetime

      });
      this.calculateRunningTime()
      this.iskh = this.calculateEff()
      this.avarageTime = this.calculateAverageTime()
      this.formatOutput();
      this.calculated = true;
    }
  }

  public calculateRunningTime() {
    this.runningTimeMs = this.dates[0] - this.dates[this.dates.length - 1];
    this.runningTime = this.msToTime(this.runningTimeMs)
  }

  public msToTime(ms: any) {
    var seconds = parseInt((ms / 1000).toFixed(1));
    var minutes = parseInt((ms / (1000 * 60)).toFixed(1));
    var hours = parseInt((ms / (1000 * 60 * 60)).toFixed(1));
    var days = parseInt((ms / (1000 * 60 * 60 * 24)).toFixed(1));
    return ({ seconds: (seconds - minutes * 60), minutes: (minutes - hours * 60), hours: (hours - days * 24), days: days })
  }

  public calculateEff() {
    return Math.round((this.totalEarned - this.firstSitePayout) * 1000 * 60 * 60 / this.runningTimeMs);
  }

  public calculateAverageTime() {
    return this.runningTimeMs / (this.totalSites - 1)
  }

  public formatOutput() {
    // Parse isk/h and total earned
    this.iskhParsed = this.iskh.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    this.totalEarnedParsed = this.totalEarned.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");

    // Parse Running Time
    if (this.runningTime.days > 0 && this.runningTime.days != undefined) {
      this.runningTimeString += this.runningTime.days + "d ";
    }
    if (this.runningTime.hours > 0 && this.runningTime.hours != undefined) {
      this.runningTimeString += this.runningTime.hours + "h ";
    }
    if (this.runningTime.minutes > 0 && this.runningTime.minutes != undefined) {
      this.runningTimeString += this.runningTime.minutes + "m ";
    }
    if (this.runningTime.seconds > 0 && this.runningTime.seconds != undefined) {
      this.runningTimeString += this.runningTime.seconds + "s ";
    }

    // Parse slowest and fastest site time
    let slowestTimeObject = this.msToTime(this.slowestSiteTime)
    if (slowestTimeObject.days > 0 && slowestTimeObject.days != undefined) {
      this.slowestTimeString += slowestTimeObject.days + "d ";
    }
    if (slowestTimeObject.hours > 0 && slowestTimeObject.hours != undefined) {
      this.slowestTimeString += slowestTimeObject.hours + "h ";
    }
    if (slowestTimeObject.minutes > 0 && slowestTimeObject.minutes != undefined) {
      this.slowestTimeString += slowestTimeObject.minutes + "m ";
    }
    if (slowestTimeObject.seconds > 0 && slowestTimeObject.seconds != undefined) {
      this.slowestTimeString += slowestTimeObject.seconds + "s ";
    }

    // Fastest Site
    let fastestTimeObject = this.msToTime(this.fastestSiteTime)
    if (fastestTimeObject.days > 0 && fastestTimeObject.days != undefined) {
      this.fastestTimeString += fastestTimeObject.days + "d ";
    }
    if (fastestTimeObject.hours > 0 && fastestTimeObject.hours != undefined) {
      this.fastestTimeString += fastestTimeObject.hours + "h ";
    }
    if (fastestTimeObject.minutes > 0 && fastestTimeObject.minutes != undefined) {
      this.fastestTimeString += fastestTimeObject.minutes + "m ";
    }
    if (fastestTimeObject.seconds > 0 && fastestTimeObject.seconds != undefined) {
      this.fastestTimeString += fastestTimeObject.seconds + "s ";
    }

    // AvarageTime 
    let averageTimeObject = this.msToTime(this.avarageTime)
    if (averageTimeObject.days > 0 && averageTimeObject.days != undefined) {
      this.averageTimeString += averageTimeObject.days + "d ";
    }
    if (averageTimeObject.hours > 0 && averageTimeObject.hours != undefined) {
      this.averageTimeString += averageTimeObject.hours + "h ";
    }
    if (averageTimeObject.minutes > 0 && averageTimeObject.minutes != undefined) {
      this.averageTimeString += averageTimeObject.minutes + "m ";
    }
    if (averageTimeObject.seconds > 0 && averageTimeObject.seconds != undefined) {
      this.averageTimeString += averageTimeObject.seconds + "s ";
    }
    this.save();
  }

  private save() {
    console.log(this.iskhParsed);
    if (this.iskhParsed == 'NaN'){
      this._api.saveCalculator(this.iskhParsed, this.totalEarnedParsed, this.runningTimeString, this.slowestTimeString, this.fastestTimeString, this.averageTimeString, this.walletInput)
      .then((response) => {
        console.log(response);
        this.router.navigate(['/fleetstats', response.id]);
      })
      .catch((err) => {
        console.error(new Error(err));
      })
    }
    else {
    this._api.saveCalculator(this.iskhParsed, this.totalEarnedParsed, this.runningTimeString, this.slowestTimeString, this.fastestTimeString, this.averageTimeString)
      .then((response) => {
        console.log(response);
        this.router.navigate(['/fleetstats', response.id]);
      })
      .catch((err) => {
        console.error(new Error(err));
      })
    }
  }

  ngOnInit() {

  }

}
