import { Component, OnInit } from '@angular/core';
import * as megafit from '../../assets/fits/megaStarterHq.json';
import { HttpClient} from '@angular/common/http';
import { waitForMap } from '@angular/router/src/utils/collection';
import { from } from 'rxjs';
import { mergeMap, tap, toArray } from 'rxjs/operators';




@Component({
  selector: 'app-dps-fits',
  templateUrl: './dps-fits.component.html',
  styleUrls: ['./dps-fits.component.scss']
})
export class DpsFitsComponent implements OnInit {
  // fits:Array<any> = [];
  // loadingStatus:boolean = false;
  // SomeVar:String = "Something";
  // constructor(private http: HttpClient) { }

  ngOnInit() {
  //   let fit = parseFit(megafit.default.fit);
  //   from(fit.modules).pipe(
  //     mergeMap(Module => this.http.get(`https://esi.tech.ccp.is/v2/search/?categories=inventory_type&search=${Module.name}&strict=true`).pipe(
  //         tap(apiResponse => Module.id = apiResponse.inventory_type[0])
  //     )),
  //     toArray()
  // ).subscribe(allResponses => {
  //     this.fits.push(fit)
  //     this.loadingStatus = true;
  //     console.log(this.fits)
  // });
  }
}

function parseFit(fit: string) {
  var moduleList: Array<any> = []
  var cargodronesList: Array<any> = []
  var shiptype: String;
  var fitName: String;

  var lines: Array<String> = [];
  var result: any = {};

  lines = fit.split("\n");
  // console.log(lines)
  lines.forEach(line => {
    if (line == "") {
      return;
    }
    if (line[0] == "[") {
      if (line.toLowerCase().includes("empty low slot") || line.toLowerCase().includes("empty med slot") || line.toLowerCase().includes("empty high slot") || line.toLowerCase().includes("empty rig slot") || line.toLowerCase().includes("empty subsystem slot"))
        return;
      if (line.includes(",")) {
        shiptype = line.split(",")[0].replace("[", "");
        fitName = line.split(", ")[1].replace("]", "");
      }
    }
    else {
      if(line.includes(",")){
        let Module = line.split(",")[0];
        let charge = line.split(",")[1];
        moduleList.push({"name": Module, "charge": charge})
      }
      else {
        var quantity = line.split(/(\sx)/)[2]
        if (line.split(/(\sx)/)[1] == " x" && !isNaN(+quantity)){
          let charge_name = line.split(/(\sx)/)[0]
          cargodronesList.push({"name": charge_name, "quantity": quantity})
        }
        else{
          moduleList.push({"name": line})
        }
      }
    }
  });
  result.ship_type = shiptype;
  result.fit_name = fitName;
  result.modules = moduleList;
  result.cargo = cargodronesList;
  return result;
}
