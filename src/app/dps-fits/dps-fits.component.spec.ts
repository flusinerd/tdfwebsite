import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DpsFitsComponent } from './dps-fits.component';

describe('DpsFitsComponent', () => {
  let component: DpsFitsComponent;
  let fixture: ComponentFixture<DpsFitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DpsFitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DpsFitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
