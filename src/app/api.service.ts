import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http:HttpClient) { }

  public saveCalculator(iskperhour, totalearned, totaltime, slowest, fastest, mean, paste?):Promise<any> {
    let bodyObject = {
      iskperhour: iskperhour,
      totalearned: totalearned,
      totaltime: totaltime,
      slowest: slowest,
      fastest: fastest,
      mean: mean,
      paste: paste
    }
    let body = new HttpParams({ fromObject: bodyObject }).toString();
    const options = {
      headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')
    };
    return this.http.post('https://ditanian.com:5002/api/calculator', body, options).toPromise();
  }

  public getStats(id):Promise<any> {
    return this.http.get('https://ditanian.com:5002/api/calculator/' + id).toPromise();
  }
}
