import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JoiningFleetComponent } from './joining-fleet.component';

describe('JoiningFleetComponent', () => {
  let component: JoiningFleetComponent;
  let fixture: ComponentFixture<JoiningFleetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JoiningFleetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JoiningFleetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
