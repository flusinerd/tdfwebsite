import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SniperFitsComponent } from './sniper-fits.component';

describe('SniperFitsComponent', () => {
  let component: SniperFitsComponent;
  let fixture: ComponentFixture<SniperFitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SniperFitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SniperFitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
