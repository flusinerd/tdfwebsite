import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component'
import { NewbroGuideComponent } from './newbro-guide/newbro-guide.component';
import { JoiningFleetComponent } from './joining-fleet/joining-fleet.component';
import { DpsFitsComponent } from './dps-fits/dps-fits.component';
import { LogiFitsComponent } from './logi-fits/logi-fits.component';
import { SniperFitsComponent } from './sniper-fits/sniper-fits.component';
import { BoosterFitsComponent } from './booster-fits/booster-fits.component';
import { GeneralFittingComponent } from './general-fitting/general-fitting.component';
import { CalculatorComponent } from './calculator/calculator.component';
import { FleetstatComponent } from './fleetstat/fleetstat.component';

const routes: Routes = [
  {path: "home", component: HomeComponent},
  {path: "guide/newbro", component: NewbroGuideComponent},
  {path: "guide/joiningfleet", component: JoiningFleetComponent},
  {path: "fits/dps", component: DpsFitsComponent},
  {path: "fits/logi", component: LogiFitsComponent},
  {path: "fits/sniper", component: SniperFitsComponent},
  {path: "fits/booster", component: BoosterFitsComponent},
  {path: "fits/general", component: GeneralFittingComponent},
  {path: "fleetstats", component: CalculatorComponent},
  {path: 'fleetstats/:id', component: FleetstatComponent},
  { path: '**', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
